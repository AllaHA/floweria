import React, { Component, Fragment } from 'react';
import { Col } from 'reactstrap';
import { Link } from 'react-router-dom';

// import './categories.scss';

class Categories extends Component{

  render(){

    return(
      <Fragment>
        <Col xs="6" md="3" className="categ-item p-0">
        <Link to='/innerPage'>
          <h1>{ this.props.name }</h1>
          <p>{ this.props.height }</p>
        </Link>
        </Col>
      </Fragment>
    )
  }
}

export default Categories;