import React, { Component } from 'react';
import { Button, Col, Modal, ModalBody, ModalFooter, ModalHeader, Row } from 'reactstrap';
import { connect } from 'react-redux';
import DatePicker from 'react-datepicker';
import PACKAGING_OPTIONS from '../../../helpers/packagingOptions';
import { changeDeliveryModalStatus, changeDate, changePackaging, changeAddress, changeQuantity } from '../../../store/modals/delivery/action';
import 'react-datepicker/dist/react-datepicker.css';

class Delivery extends Component {

  render() {
    const { changeStatus, changeDate, changePackaging, changeAddress, changeQuantity } = this.props;
    const { deliveryDate, price, totalPrice, status, deliveryAddress } = this.props.delivery;

    return (
      <Modal isOpen={ status } toggle={ changeStatus }>
        <ModalHeader>Modal title</ModalHeader>
        <ModalBody>
          <Row>
            <Col xs='12' className='modal-item text-center mb-3 p-4'>
              <h6 className='text-center'>Pick a Delivery Day</h6>
              <DatePicker selected={ deliveryDate } onChange={ changeDate }/>
            </Col>
            <Col xs='12' className='p-0 modal-item p-4'>
              <h6 className='text-center'>Preferred packaging</h6>
              <div className='text-center'>
                {
                  PACKAGING_OPTIONS.map( (item, i) => {
                    return (
                      <label className='packaging' key={ i }>
                        { item.name }
                        <input onChange={ changePackaging } type='radio' value={ i } className='option-input radio' name='example'/>
                      </label>
                    )
                  })
                }
              </div>
            </Col>
            <Col xs='12' className='p-0 modal-item p-4 text-center'>
              <h6 className='text-center'>Delivery Address</h6>
              <input id='address' ref='address' type='text' value={ deliveryAddress } onChange={ changeAddress }/>
              <h6 className='mt-3'>Quantity</h6>
              <input type='number' id='quantity' ref='quantity' onInput={ changeQuantity }/>
              <div>Price: { price } $</div>
              <p className='text-center mt-4' >Total price amount including delivery</p>
              <div>{ totalPrice }</div>
            </Col>
          </Row>
        </ModalBody>
        <ModalFooter>
          <Button color='info' onClick={ changeStatus }>Save</Button>
          <Button color='secondary' onClick={ changeStatus }>Cancel</Button>
        </ModalFooter>
      </Modal>
    );
  }
}

const mapStateToProps = state => ({
  delivery: state.delivery,
});

const mapDispatchToProps = dispatch => ({
  changeStatus: () => dispatch(changeDeliveryModalStatus),
  changeDate: elem => dispatch(changeDate(elem)),
  changePackaging: elem => dispatch(changePackaging(elem)),
  changeAddress: elem => dispatch(changeAddress(elem)),
  changeQuantity: elem => dispatch(changeQuantity(elem)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Delivery);