import React, { Component } from 'react';
import {
  Button,
  Col,
  Form,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
} from 'reactstrap';
import { connect } from 'react-redux';
import {
  changeLoginModalStatus,
  changeLoginModalInfo,
  insertToken,
  goToRegModal,
} from '../../../store/modals/login/action';
import 'react-datepicker/dist/react-datepicker.css';
import styled from 'styled-components';

class Login extends Component {

  render() {
    const { phoneOrEmail, password, status } = this.props.login;
    const { changeLoginModalStatus, changeLoginModalInfo, insertToken, goToRegModal } = this.props;

    return (
      <Form>
        <Modal
          isOpen={ status }
          toggle={ changeLoginModalStatus }
        >
          <ModalHeader>Login title</ModalHeader>
          <ModalBody>
            <Row>
              <Col
                className="modal-item p-4"
                xs="12"
              >
                <Label>
                  <span>Phone or Email:</span>
                  <RegInput
                    name="phoneOrEmail"
                    value={ phoneOrEmail }
                    onChange={ changeLoginModalInfo }
                    type="text"
                  />
                </Label>
                <Label>
                  <span>Password:</span>
                  <RegInput
                    name="password"
                    value={ password }
                    onChange={ changeLoginModalInfo }
                    type="password"
                  />
                </Label>
                <span>You have not registered ? </span>
                <RegistrationSpan onClick={ goToRegModal }>Registration</RegistrationSpan>
              </Col>
            </Row>
          </ModalBody>
          <ModalFooter>
            <LoginButton onClick={ changeLoginModalStatus }>Cancel</LoginButton>
            <LoginButton onClick={ insertToken }>Send</LoginButton>
          </ModalFooter>
        </Modal>
      </Form>
    );
  }
}

const Label = styled.label`
  width: 100%;
`;

const RegInput = styled.input`
  width: 100%;
`;

const LoginButton = styled(Button).attrs({
  color: 'secondary',
})``;

const RegistrationSpan = styled.span`
  cursor: pointer;
  &:hover {
    color: blue;
    text-decoration: underline;
  }
`;

const mapStateToProps = state => ({
  login: state.login,
});

const mapDispatchToProps = dispatch => ({
  changeLoginModalStatus: () => dispatch(changeLoginModalStatus),
  changeLoginModalInfo: elem => dispatch(changeLoginModalInfo(elem)),
  insertToken: () => dispatch(insertToken()),
  goToRegModal: () => dispatch(goToRegModal()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);