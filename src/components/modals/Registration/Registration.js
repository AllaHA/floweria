import React, { Component } from 'react';
import { Button, Col, Modal, ModalBody, ModalFooter, ModalHeader, Row } from 'reactstrap';
import { connect } from 'react-redux';
import { changeRegInfo, backLoginModal, cancelRegistrationModal, sendRegistrationModal } from '../../../store/modals/registration/action';
import 'react-datepicker/dist/react-datepicker.css';
import styled from 'styled-components';

class Registration extends Component {

  render() {
    const { changeRegInfo, backLoginModal, cancelRegistrationModal, sendRegistrationModal } = this.props;
    const { statusReg, firstNameReg, passwordReg, confirmPasswordReg, emailOrPhone } = this.props.registration;

    return (
      <Modal
        isOpen={ statusReg }
        toggle={ cancelRegistrationModal }
      >
        <ModalHeader>Registration title</ModalHeader>
        <ModalBody>
          <Row>
            <Col
              className="modal-item p-4"
              xs="12"
            >
              <Label>
                <span>Email or Phone:</span>
                <RegInput
                  name="emailOrPhone"
                  value={ emailOrPhone }
                  onChange={ changeRegInfo }
                  type="text"
                />
              </Label>
              <Label>
                <span>Full Name:</span>
                <RegInput
                  name="fullNameReg"
                  value={ firstNameReg }
                  onChange={ changeRegInfo }
                  type="text"
                />
              </Label>
              <Label>
                <span>Password:</span>
                <RegInput
                  name="passwordReg"
                  value={ passwordReg }
                  onChange={ changeRegInfo }
                  type="password"
                />
              </Label>
              <Label>
                <span>Confirm Password:</span>
                <RegInput
                  name="confirmPasswordReg"
                  value={ confirmPasswordReg }
                  onChange={ changeRegInfo }
                  type="password"
                />
              </Label>
            </Col>
          </Row>
        </ModalBody>
        <ModalFooter>
          <RegButton onClick={ backLoginModal }>Back</RegButton>
          <RegButton onClick={ cancelRegistrationModal }>Cancel</RegButton>
          <RegButton onClick={ sendRegistrationModal }>Send</RegButton>
        </ModalFooter>
      </Modal>
    );
  }
}

const RegButton = styled(Button).attrs({
  color: 'secondary'
})`
`;

const Label = styled.label`
  width: 100%;
`;

const RegInput = styled.input`
  width: 100%;
`;

const mapStateToProps = state => ({
  registration: state.registration,
});

const mapDispatchToProps = dispatch => ({
  backLoginModal: () => dispatch(backLoginModal()),
  cancelRegistrationModal: () => dispatch(cancelRegistrationModal),
  sendRegistrationModal: () => dispatch(sendRegistrationModal()),
  changeRegInfo: elem => dispatch(changeRegInfo(elem)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Registration);