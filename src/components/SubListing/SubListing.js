import React, { Fragment, Component } from 'react';
import { Col } from 'reactstrap';

// import './subListing.scss';

class SubListing extends Component {
  state = {
        
  };

  render(){

    return(
      <Fragment>
        <Col xs='3' className="sub-item">
          <h1>{ this.props.name }</h1>
        </Col>
      </Fragment>
    )
  }
}

export default SubListing;