import React from 'react';
import TopBarProgress from 'react-topbar-progress-indicator';
import { connect } from 'react-redux';
import progressConfig from './progressBarStyle';

export class ProgressBar extends React.Component {

  render() {
    const { progressBarStatus } = this.props;

    if(progressBarStatus === true) {
      return( <TopBarProgress /> )
    } else {
      return('');
    }
  }
}

TopBarProgress.config(progressConfig);

const mapStateToProps = state => ({
  progressBarStatus: state.progress.progressBarStatus
});

export const HandleProgressBar = connect(mapStateToProps,null)(ProgressBar);