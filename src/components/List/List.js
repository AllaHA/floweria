import React, { Component } from 'react';
import styled from 'styled-components';
import { Col } from 'reactstrap';

class List extends Component {

  render() {
    const item = this.props.item;

    return (
      <Col lg="12" className="item list-item">
        <div className="innerContent">
          <Image src={ item.photo } alt="flowers"/>
          <p>
            <span className="listPageItemSpam">{ item.name }</span>
          </p>
        </div>
      </Col>
    )
  }
}

const Image = styled.img`
  height: 100px;
  width: 100px;
  object-fit: cover;
`;

export default List;