import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Row, Col, Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { HandleProgressBar } from '../Progress/ProgressBar';
import { changeLoginModalStatus } from '../../store/modals/login/action';
import LoginModal from '../modals/Login/Login';
import RegistrationModal from '../modals/Registration/Registration';

class MainMenu extends Component {

  render() {

    return (
      <>
        <HandleProgressBar />
        <LoginModal />
        <RegistrationModal />

        <MainMenuContainer className='container-fluid container'>
          <MenuFlex>
            <FlexFloweria>
              <ButtonParent>
                <Button onClick={this.props.changeLoginModalStatus}>Login</Button>
              </ButtonParent>
              Floweria
            </FlexFloweria>
            <FlexFlowerShop>flowershop</FlexFlowerShop>
          </MenuFlex>
          <Row className='justify-content-center'>
            <Col xs='auto'>
              <Link to='/'>
                <LinkSpan>Home</LinkSpan>
              </Link>
            </Col>
            <Col xs='auto'>
              <Link to='/about'>
                <LinkSpan>About</LinkSpan>
              </Link>
            </Col>
            <Col xs='auto'>
              <Link to='/code'>
                <LinkSpan>Code</LinkSpan>
              </Link>
            </Col>
            <Col xs='auto'>
              <Link to='/contacts'>
                <LinkSpan>Contact</LinkSpan>
              </Link>
            </Col>
          </Row>
        </MainMenuContainer>
      </>
    );
  }
}

const MainMenuContainer = styled.div`
  a{
    border-bottom: 1px solid transparent;
    padding-bottom: 6px;
    color: #333333;
    &:hover{
       color: #dc3e66;
    }
  }
`;

const ButtonParent = styled.div`
  text-align: right;
`;

const MenuFlex = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 30px;
`;

const FlexFloweria = styled.div`
  text-align: center;
  font-size: 50px;
  line-height: 1;
`;

const FlexFlowerShop = styled.div`
  text-align: center;
  font-size: 14px;
  font-weight: light;
  letter-spacing: 4px;
`;

const LinkSpan = styled.span``;

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => ({
  changeLoginModalStatus: () => dispatch(changeLoginModalStatus),
});

export default connect(mapStateToProps, mapDispatchToProps)(MainMenu);