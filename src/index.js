import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { BrowserRouter } from 'react-router-dom';
import Routes from './routers/index';
import reducer from './store/index';
import 'bootstrap/dist/css/bootstrap.css';

const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)));

render(
  (<Provider store={ store }>
    <BrowserRouter>
      <Routes/>
    </BrowserRouter>
  </Provider>), document.getElementById('root')
);