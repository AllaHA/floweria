import { combineReducers } from 'redux';

import { app } from './app/reducer';
import { progress } from './progress/reducer';
import { delivery } from './modals/delivery/reducer';
import { login } from './modals/login/reducer';
import { registration } from './modals/registration/reducer';
// import { home } from './home/reducer';
// import { secondary } from './secondary/reducer';

export default combineReducers({
  app,
  progress,
  delivery,
  login,
  registration,
});