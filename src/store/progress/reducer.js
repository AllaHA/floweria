const intialState = {
  progressBarStatus: false,
};

export const progress = (state = intialState, action) => {
  switch (action.type) {
    case 'SET_PROGRESS_BAR':
      return {
        ...state,
        ...state.progressBarStatus = action.isStatus
      };
    default:
      return state;
  }
};