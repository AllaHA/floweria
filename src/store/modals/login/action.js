import FetchApi from "../../../helpers/fetchApi";

export const changeLoginModalStatus = {
  type: 'CHANGE_LOGIN_MODAL_STATUS'
};

export const insertToken = () => async (dispatch, getState) => {
  const { phoneOrEmail, password } = getState().login;
  try {
    const res = await FetchApi.post(`/users/login`,
      { username: phoneOrEmail, password }
    );
    dispatch({
      type: 'USER_LOGIN',
      value: res.data.userData,
    });
    dispatch({
      type: 'CHANGE_LOGIN_MODAL_STATUS'
    });
    localStorage.setItem('access_token', res.data.token)
  } catch (e) {
    console.log(e);
    dispatch({
      type: 'USER_LOGIN_ERROR',
      value: e,
    });
  }
};

export const goToRegModal = () => async (dispatch) => {
  dispatch(changeLoginModalStatus);
  dispatch({
    type: 'CHANGE_REG_MODAL_STATUS',
  })
};

export const changeLoginModalInfo = (elem) => ({
  type: 'CHANGE_LOGIN_MODAL_INFO',
  name: elem.target.name,
  value: elem.target.value,
});