const initialState = {
  status: false,
  phoneOrEmail: '',
  password: '',
  user: {},
};

export const login = (state = initialState, action) => {
  switch (action.type) {
    case 'CHANGE_LOGIN_MODAL_STATUS':
      return {
        status: !state.status,
        phoneOrEmail: '',
        password: '',
      };
    case 'CHANGE_LOGIN_MODAL_INFO':
      return {
        ...state,
        [action.name]: action.value,
      };
    case 'USER_LOGIN':
      return {
        ...state,
        user: action.value,
      };
    default:
      return state
  }
};