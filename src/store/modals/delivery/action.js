import packaging from '../../../helpers/packagingOptions';

export const changeDeliveryModalStatus = {
  type: 'CHANGE_DELIVERY_MODAL_STATUS',
};

export const changeDate = elem => {
  return {
    type: 'CHANGE_DELIVERY_MODAL_DATE',
    value: elem
  };
};

export const changePackaging = elem => {
  let valuePackaging;
  packaging.forEach(item => {
    if (item.id === Number(elem.target.value)) {
      valuePackaging = item
    }
  });

  return {
    type: 'CHANGE_DELIVERY_MODAL_PACKAGING',
    value: valuePackaging,
  };
};

export const changeAddress = elem => {
  return {
    type: 'CHANGE_DELIVERY_MODAL_ADDRESS',
    value: elem.target.value,
  };
};

export const changeQuantity = elem => {
  return {
    type: 'CHANGE_DELIVERY_MODAL_QUANTITY',
    value: elem.target.value,
  };
};