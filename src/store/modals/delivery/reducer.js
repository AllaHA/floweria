const initialState = {
  status: false,
  totalPrice: 0,
  price: 3,
  deliveryAddress: '',
  packaging: null,
  deliveryDate: null,
};

export const delivery = (state = initialState, action) => {
  switch (action.type) {
    case 'CHANGE_DELIVERY_MODAL_STATUS':
      return {
        ...state,
        ...state.status = !state.status
      };
    case 'CHANGE_DELIVERY_MODAL_DATE':
      return {
        ...state,
        ...state.deliveryDate = action.value,
      };
    case 'CHANGE_DELIVERY_MODAL_PACKAGING':
      return {
        ...state,
        ...state.packaging = action.value
      };
    case 'CHANGE_DELIVERY_MODAL_ADDRESS':
      return {
        ...state,
        ...state.deliveryAddress = action.value
      };
    case 'CHANGE_DELIVERY_MODAL_QUANTITY':
      return {
        ...state,
        ...state.totalPrice = state.price * action.value
      };
    default: {
      return state
    }
  }
};