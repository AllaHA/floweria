export const changeRegInfo = elem => ({
  type: 'CHANGE_REG_MODAL_INFO',
  name: elem.target.name,
  value: elem.target.value,
});

export const cancelRegistrationModal = ({
  type: 'CHANGE_REG_MODAL_STATUS'
});

export const sendRegistrationModal = () => dispatch => {
  localStorage.setItem('access_token', 'asdasdasdasd');
  dispatch(cancelRegistrationModal);
  dispatch({
    type: 'CHANGE_DELIVERY_MODAL_STATUS',
  })
};

export const backLoginModal = () => async (dispatch) => {
  dispatch(cancelRegistrationModal);
  dispatch({
    type: 'CHANGE_LOGIN_MODAL_STATUS',
  });
};