const initialState = {
  statusReg: false,
  emailOrPhone: '',
  fullNameReg: '',
  passwordReg: '',
  confirmPasswordReg: '',
};

export const registration = (state = initialState, action) => {
  switch (action.type) {
    case 'CHANGE_REG_MODAL_STATUS':
      return {
        statusReg: !state.statusReg,
        emailOrPhone: '',
        fullNameReg: '',
        passwordReg: '',
        confirmPasswordReg: '',
      };
    case 'CHANGE_REG_MODAL_INFO':
      return {
        ...state,
        ...state[action.name] = action.value,
      };
    default:
      return state
  }
};