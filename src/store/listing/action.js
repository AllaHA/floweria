export const changeModalStatus = () => dispatch => {
  if (localStorage.getItem('access_token')) {
    dispatch({
      type: 'CHANGE_DELIVERY_MODAL_STATUS',
    });
  } else {
    dispatch({
      type: 'CHANGE_LOGIN_MODAL_STATUS',
    });
  }
};