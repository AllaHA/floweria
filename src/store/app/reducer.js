const intialState = {
  categories: {},
  listings: [],
};

export const app = (state = intialState, action) => {
  switch (action.type) {
    case 'GET_CATEGORIES':
      return{
        ...state,
        categories: action.value
      };
    case 'GET_LISTINGS':
      return{
        ...state,
        listings: action.value
      };
    case 'GET_SUBCATEGORIES':
      return{
        ...state,
        subCategories: action.value
      };
    default: {
      return state
    }
  }
};