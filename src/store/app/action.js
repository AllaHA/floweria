import FetchApi from '../../helpers/fetchApi';

export const getMainCategory = () => async (dispatch) => {
  dispatch({
    type: 'SET_PROGRESS_BAR',
    isStatus: true,
  });
  try {
    const res = await FetchApi.get(`/categories/main`);
    dispatch({
      type: 'GET_CATEGORIES',
      value: res.data,
    });
  } catch (e) {
    dispatch({
      type: 'GET_CATEGORIES_ERROR',
      value: e,
    });
  }
  setTimeout(() => {
    dispatch({
      type: 'SET_PROGRESS_BAR',
      isStatus: false,
    });
  }, 800);
};

export const getListings = arg => async (dispatch) => {
  dispatch({
    type: 'SET_PROGRESS_BAR',
    isStatus: true,
  });
  try {
    const res = await FetchApi.get(`/search/tag/${arg}`);
    dispatch({
      type: 'GET_LISTINGS',
      value: res.data.listings,
    });
  } catch (e) {
    dispatch({
      type: 'GET_LISTINGS_ERROR',
      value: e,
    });
  }
  setTimeout(() => {
    dispatch({
      type: 'SET_PROGRESS_BAR',
      isStatus: false,
    });
  }, 800);
};

export const openDeliveryModal = {
  type: 'CHANGE_DELIVERY_MODAL_STATUS',
};





export const asdasdasd = pathName => async (dispatch) => {
  // dispatch({
  //   type: 'SET_PROGRESS_BAR',
  //   isStatus: true,
  // });
  // try {
  //   let listingsObj = {};
  //   let pathNameArr = pathName.split('/');
  //   const listing_id = pathNameArr[pathNameArr.length - 1];
  //   const sub_id = pathNameArr[pathNameArr.length - 3];
  //   const res = await FetchApi.get(`http://0a6504ae.ngrok.io/search/byType/${ listing_id }`);
  //   listingsObj.listingsArr = res.data.listings;
  //
  //   res.data.listings.forEach(item => {
  //     if (item.listingSubCategory === listing_id && item.listingType === sub_id) {
  //       item.listingTypeProperties.subCategories.forEach(subCat => {
  //         if (subCat.id === listing_id) {
  //           listingsObj.listingsHeadline = subCat.name;
  //         }
  //       })
  //     }
  //   });
  //
  //   dispatch({
  //     type: 'GET_LISTINGS',
  //     value: listingsObj,
  //   });
  // } catch (e) {
  //   dispatch({
  //     type: 'GET_CATEGORIES_ERROR',
  //     value: e,
  //   });
  // }
  // setTimeout(() => {
  //   dispatch({
  //     type: 'SET_PROGRESS_BAR',
  //     isStatus: false,
  //   });
  // }, 800);
};