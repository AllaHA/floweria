import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import styled from 'styled-components';
import List from '../../components/List/List';
import { getMainCategory } from '../../store/app/action';

class Home extends Component{

  componentDidMount() {
    const { categories } = this.props.app;
    !categories.main && this.props.getMainCategory();
  }

  render() {
    const { progressBarStatus } = this.props.progress;
    const { categories } = this.props.app;

    if (!progressBarStatus){
      return (
        <>
          <HomeContent>
            <Row className='justify-content-around'>
              <Col md='7'>
                <div className='text-content'>
                  <BgcBoldText>Happiness</BgcBoldText>
                  <Heading>
                    <HeadingSpan >The most beautiful way to send</HeadingSpan>
                    <HeadingSpan>flowers</HeadingSpan>
                  </Heading>
                  <Paragraph>
                    Flowers, they are not only plants that grow everywhere, they are also a
                    universal gift to your girlfriend or to you mother... or to.. it could be tons of options. But now, in
                    21h century there is a possibility to send electronic flowers to anyone you want. I'll show you how type
                    all that flower symbols directly from your keyboard and a lot of another interesting things.
                  </Paragraph>
                  <div>
                    <OrderBtn>Order Now</OrderBtn>
                  </div>
                </div>
              </Col>
              <BannerImgWrapper className='col' md='4'>
                <Image src={ require('../../assets/Images/banner.png') } alt='flower'/>
              </BannerImgWrapper>
            </Row>
          </HomeContent>
          <ListContent className='list-content'>
            <Container className='home'>
              <Header1>Flower Categories</Header1>
              <Row>
                {categories.categoryOptions && categories.categoryOptions.map((item, i) => (
                  <Link key={ i } to={{ pathname: item.id, categoryOptionId: item.id }}>
                    <List item={ item.properties }/>
                  </Link>
                ))}
              </Row>
            </Container>
          </ListContent>
        </>
      )
    } else {
      return('');
    }
  }
}

const HomeContent = styled.div.attrs({
  className: 'container',
})`
  margin-top: 80px;  
`;

const BgcBoldText = styled.h1`
  position: absolute;
  font-size: 136px;
  color: rgb(225, 226, 228);
  left: 10px;
  top: -22px;
  line-height: 0.4;
  z-index: -1;
`;

const Heading = styled.h4`
  font-size: 14px;
  color: rgb(152, 149, 149);
  margin-top: 8px;
  margin-bottom: 60px;
`;

const Paragraph = styled.p`
  color: rgb(152, 149, 149);
  font-size: 14px;
`;

const OrderBtn = styled.button`
  background-color: rgb(220, 62, 102) !important;
  color: #fff !important;
  cursor: pointer;
  margin-top: 20px;
  padding: 6px 22px;
  border: none;
  &:focus {
    outline: none;
  }
  &:hover {
    box-shadow: 1px 1px 4px 2px rgba(220, 62, 102, 0.35);
  }
`;

const BannerImgWrapper = styled.div`
  height: 100%;
  padding: 0;
`;

const Image = styled.img`
  height: 100%;
  width: 100%;
  object-fit: cover;
`;

const ListContent = styled.div`
  margin-top: 20px;
`;

const Header1 = styled.h1`
  text-align: center
`;

const HeadingSpan = styled.span`
  &:nth-child(1) {
    font-size: 22px;
  }
  &: nth-child(2) {
    color: rgb(220, 62, 102);
    position: absolute;
    left: 52%;
    top: 0;
    letter-spacing: 7px;
    font-size: 34px;
  }
`;

const mapStateToProps = state => ({
  app: state.app,
  progress: state.progress,
});

const mapDispatchToProps = dispatch => ({
  getMainCategory: () => dispatch(getMainCategory()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);