import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Container, Col } from 'reactstrap';
import { getListings, openDeliveryModal } from '../../store/app/action';
import styled from 'styled-components';
import DeliveryModal from '../../components/modals/Delivery/Delivery';

class Secondary extends Component {

  componentDidMount() {
    const { location: { pathname }, getListings } = this.props;
    getListings(pathname.substr(1, pathname.length));
  };

  render(){
    const { progress, listings } = this.props;

    if (!progress.progressBarStatus){
      return(
        <Container>
          <DeliveryModal />

          {listings.map(( item, i ) =>(
            <DivCol
              lg='3'
              key={ i }
            >
              <div>
                <Image src={ item.photo } alt='flowers'/>
                <p>
                  <DivColSpan
                    onClick={this.props.openDeliveryModal}
                  >
                    { item.name }
                  </DivColSpan>
                </p>
              </div>
            </DivCol>
          ))}
        </Container>
      )
    } else {
      return('')
    }
  }
}
//example
// const DivContainer = styled(Container).attrs({
//   className: 'divContainerBig',
// })`
//   border: 2px solid red;
// `;

const DivCol = styled(Col)`
  display: inline-block;
`;

const DivColSpan = styled.span`
  display: inline-block;
  cursor: pointer;
  &:hover {
    color: blue;
    text-decoration: underline;
  }
`;

const Image = styled.img`
  height: 100%;
  width: 100%;
  object-fit: cover;
`;

const mapStateToProps = state => ({
  listings: state.app.listings,
  progress: state.progress,
});

const mapDispatchToProps = dispatch => ({
  getListings: arg => dispatch(getListings(arg)),
  openDeliveryModal: () => dispatch(openDeliveryModal),
});

export default connect(mapStateToProps, mapDispatchToProps)(Secondary);