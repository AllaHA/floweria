import React, { Component } from 'react';
import { Container, Row } from 'reactstrap';
import DeliveryModal from "../../components/modals/Delivery/Delivery";

// import './individual.scss';
import 'react-datepicker/dist/react-datepicker.css';

class Individual extends Component {
  state = {
    deliveryModalIndividualList: false
  };

  toggleDeliveryModalIndividualList = () => {
    this.setState(state => {
      return { deliveryModalIndividualList: !state.deliveryModalIndividualList };
    });
  };

  render() {
    const { deliveryModalIndividualList } = this.state;

    return(
      <div>
        <Container className="mt-5 individual-list-wrapper">
          <h2>Individual page of flower list</h2>
          <Row className="justify-content-center">
            <div className="item p-0">
              <div className="item-child"><img src={ require('../../assets/Images/birthday-bouquet.jpg') } alt="img"/></div>
              <p className="item-child">The Birthday Brights™ Bouquet is a true celebration of color and life to surprise and
                  delight your special recipient on their big day! Hot pink gerbera daisies and orange roses
                  take center stage surrounded by purple gilly flowers, yellow chrysanthemums, orange
                  carnations, green button poms, bupleurum, and lush greens to create party perfect
                  birthday display. Presented in a modern rectangular ceramic vase with colorful 
                  striping at the bottom, "Happy Birthday" lettering at the top, and a bright pink bow 
                  at the center, this unforgettable fresh flower arrangement is then accented with a 
                  striped happy birthday pick to create a fun and festive gift. BETTER bouquet is approx..
                  16"H x 12"W.
                </p>
            </div>
            <div className="order-btn-wrapper">
              <button className="order" onClick={ this.toggleDeliveryModalIndividualList }>Make an order</button>
            </div>
          </Row>
        </Container>

        <DeliveryModal deliveryModal={ deliveryModalIndividualList } toggleDeliveryModal={ this.toggleDeliveryModalIndividualList }/>
      </div>
    )
  }
}

export default Individual;