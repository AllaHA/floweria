import React, { Component, Fragment } from 'react';
import { Container } from 'reactstrap';
// import { Col, Container, Row } from 'reactstrap';
// import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { asdasdasd } from '../../store/app/action';
import { changeModalStatus } from '../../store/listing/action';
import DeliveryModal from '../../components/modals/Delivery/Delivery';
import LoginModal from '../../components/modals/Login/Login';
import RegistrModal from '../../components/modals/Registration/Registration';

class Listing extends Component {

  componentDidMount() {
    const { pathname } = this.props.location;
    this.props.asdasdasd(pathname);
  };

  render(){
    const { progress, app } = this.props;
    const { listingsHeadline } = app.listings;

    if (!progress.progressBarStatus){
    return(
      <Fragment>
        <Container className='listing'>
          <h1>{ listingsHeadline }</h1>
          {/*<Row>*/}
          {/*{*/}
          {/*  listingsArr.map(( item, i ) => (*/}
          {/*    <Col lg='3' className='item listing-item' key={ i }>*/}

          {/*      <Link to={{ pathname: '/Internal' }}/>*/}
          {/*      <div className='innerContent'>*/}
          {/*        <img className='innerContentImg' onClick={ this.props.changeModalStatus } src={ item.photo } alt='flowers'/>*/}
          {/*        <p className='innerContentImgP'>*/}
          {/*          <span className='innerContentImg'>{ item.name }</span>*/}
          {/*          <span className='innerContentSpan'>Գինը՝ { item.price } դրա</span>*/}
          {/*        </p>*/}
          {/*      </div>*/}
          {/*    </Col>*/}
          {/*  ))*/}
          {/*}*/}
          {/*</Row>*/}
          <DeliveryModal />
          <LoginModal />
          <RegistrModal />
        </Container>
      </Fragment>
    ) } else {
      return('')
    }
  }
}

const mapStateToProps = state => ({
  app: state.app,
  progress: state.progress,
});

const mapDispatchToProps = {
  asdasdasd,
  changeModalStatus,
};

export default connect(mapStateToProps, mapDispatchToProps)(Listing);