import React, { Component, Fragment } from 'react';
import { Col, Row, Container } from 'reactstrap';
import { Link } from 'react-router-dom';

// import './internal.scss';

class Internal extends Component{

  render(){

    return(
      <Fragment>
        <Row className="justify-content-center">
          <Col xs="1" className="inner-item"><Link to='/bouquet'>Bouquet</Link></Col>
          <Col xs="1" className="inner-item"><Link to='/bouquet'>Single</Link></Col>
        </Row>
        <Container className="mt-5">
          <Row className="justify-content-between">
            <Col xs="3" md="3" className="categ-item p-0 inner-list">
               <h4>Vard spitak</h4>
            </Col>
            <Col xs="3" md="3" className="categ-item p-0 inner-list">
               <h4>Vard dexin</h4>
            </Col>
            <Col xs="3" md="3" className="categ-item p-0 inner-list">
               <h4>Vard karmir</h4>
            </Col>
          </Row>
        </Container>
      </Fragment>
    )
  }
}

export default Internal;