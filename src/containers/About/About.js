import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Container, Row, Col } from 'reactstrap';

class About extends Component{

  render(){
    return (
      <Container>
        <Row>
          <Col><h1>About</h1></Col>
        </Row>
      </Container>
    )
  }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(About);