import React, { Component, Fragment } from 'react';
// import './bouquet.scss';
import { Row } from 'reactstrap';
import SubListing from '../../components/SubListing/SubListing';

class Bouquet extends Component {
  state = {
    SubListing: [
      { name: 'dexin vard' },
      { name: 'karmir vard' },
      { name: 'spitak vard' },
    ]
  };

  render() {
    return (
      <Fragment>
        <Row className="justify-content-center mt-5">
          {
            this.state.SubListing.map(( list, i) => {
              return <SubListing name={ list.name } key={ i }/>
            })
          }
        </Row>
      </Fragment>
    )
  }
}

export default Bouquet;