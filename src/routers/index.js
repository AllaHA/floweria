import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import MainMenu from '../components/MainMenu/MainMenu';
import About from '../containers/About/About';
import Home from '../containers/Home/Home';
import Code from '../containers/Code/Code';
import Contacts from '../containers/Contacts/Contacts';
import Internal from '../containers/Internal/Internal';
import Bouquet from '../containers/Bouquet/Bouquet';
import Secondary from '../containers/Secondary/Secondary';
import Individual from '../containers/Individual/Individual';
import Listing from '../containers/Listing/Listing';

export default class Index extends Component {
  render() {
    return (
      <>
        <MainMenu />
        <Switch>
          <Route exact path='/about' component={ About }/>
          <Route exact path='/contacts' component={ Contacts }/>
          <Route exact path='/code' component={ Code }/>
          <Route exact path='/individual' component={ Individual }/>
          <Route exact path='/internal' component={ Internal }/>
          <Route exact path='/bouquet' component={ Bouquet }/>
          <Route exact path='/:secondary/item/:listing' component={ Listing }/>

          <Route exact path='/:secondary' component={ Secondary }/>
          <Route path='/' component={ Home }/>
        </Switch>
      </>
    );
  }
}