export const getShortAvatarText = (str) => {
  let res = str.charAt(0);

  return res;
};

export const getAvatarColor = (str) => {
  let point;
  str='r';
  if (str.charCodeAt() <= 50) {
    point = str.charCodeAt()
  } else if (str.charCodeAt() > 50 && str.charCodeAt() <= 100) {
    point = str.charCodeAt() - 50
  } else if (str.charCodeAt() > 100 && str.charCodeAt() <= 150) {
    point = str.charCodeAt() - 100
  } else {
    point = str.charCodeAt() - 150
  }

  let r = '#' + (point/50 * 0xFFFFFF<<0).toString(16);
  return r;

};